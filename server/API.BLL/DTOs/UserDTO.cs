﻿using System.Text.Json.Serialization;

namespace API.BLL.DTOs;

public sealed class UserDTO
{
    public int Id { get; set; }

    public int? TeamId { get; set; }
    [JsonIgnore]
    public TeamDTO? Team { get; set;}
    
    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? Email { get; set; }
    
    public DateTime CreatedAt { get; set; }
        
    public DateTime BirthDay { get; set; }

}