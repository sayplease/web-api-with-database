﻿namespace API.BLL.DTOs;

public class UserTeamDTO
{
    public int Id { get; set; }
    public string Name { get; set; }
    
    public List<UserDTO> Users { get; set; }
}