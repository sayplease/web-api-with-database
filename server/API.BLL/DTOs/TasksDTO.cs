﻿using System.Text.Json.Serialization;
using API.DAL.EnumState;


namespace API.BLL.DTOs;

public sealed class TaskDTO
{
    public int Id { get; set; }

    public int ProjectId { get; set; }
      
    public ProjectDTO Project { get; set; }
        
    public int PerformerId { get; set; }
        
    public UserDTO? Performer { get; set; }

    public string? Name { get; set; }

    public string? Description { get; set; }

    public TaskState? State { get; set; }
        
    public DateTime? FinishedAt { get; set; }

}