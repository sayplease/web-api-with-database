﻿using System.Text.Json.Serialization;

namespace API.BLL.DTOs;

public sealed class TeamDTO
{
    public int Id { get; set; }

    public string Name { get; set; }
    
   public DateTime CreateAt { get; set; }
   
   public List<UserDTO> Users { get; set; }

}