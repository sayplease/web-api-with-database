﻿using API.BLL.DTOs;
using AutoMapper;

namespace API.BLL.MapingProfile;

public sealed class TaskProfile : Profile
{
    public TaskProfile()
    {
        CreateMap<API.DAL.Entities.Task, TaskDTO>();
        CreateMap<TaskDTO, API.DAL.Entities.Task>();
    }
}