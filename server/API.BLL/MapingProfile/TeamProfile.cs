﻿using API.BLL.DTOs;
using API.DAL.Entities;
using AutoMapper;


namespace API.BLL.MapingProfile;

public sealed class TeamProfile : Profile
{
    public TeamProfile()
    {
        CreateMap<Team, TeamDTO>();
        CreateMap<TeamDTO, Team>();
        CreateMap<Team, UserTeamDTO>();
    }
}