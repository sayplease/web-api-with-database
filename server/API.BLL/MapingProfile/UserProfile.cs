﻿using API.BLL.DTOs;
using API.DAL.Entities;
using AutoMapper;


namespace API.BLL.MapingProfile;

public sealed class UserProfile : Profile
{
    public UserProfile()
    {
        CreateMap<User, UserDTO>();
        CreateMap<UserDTO, User>();
    }
}