﻿using API.BLL.DTOs;
using API.DAL.Entities;
using AutoMapper;


namespace API.BLL.MapingProfile;

public sealed class ProjectProfile : Profile
{
    public ProjectProfile()
    {
        CreateMap<Project, ProjectDTO>();
        CreateMap<ProjectDTO, Project>();
    }
}