﻿using API.BLL.DTOs;
using API.BLL.Services.Abstract;
using API.DAL.Context;
using API.DAL.Entities;
using API.DAL.Repository;
using API.DAL.Repository.User;
using API.DAL.UnitOfWork;
using AutoMapper;

namespace API.BLL.Services;

public class UserService : BaseService
{
    

    public UserService(IMapper mapper, IUnitOfWork unitOfWork) : base(mapper, unitOfWork)
    {
        
    }

    public async Task<List<UserDTO>> GetAll()
    {
        var users = await _unitOfWork.Users.GetAll();
        
        return _mapper.Map<List<UserDTO>>(users);
    }

    public async Task<UserDTO> GetById(int id)
    {
        var user = await _unitOfWork.Users.GetById(id);

        return _mapper.Map<UserDTO>(user);
    }

    public async Task<bool> Create(UserDTO newUserDto)
    {
        var newUser = _mapper.Map<User>(newUserDto);
       await _unitOfWork.Users.Create(newUser);
       await _unitOfWork.CompleteAsync();
       return true;
    }
    
    public async Task<bool> Update(UserDTO updUserDto)
    {
        var updUser = _mapper.Map<User>(updUserDto);
        
        await _unitOfWork.Users.Update(updUser);
        await _unitOfWork.CompleteAsync();
        return true;
    }

    public async Task<bool> Delete(int id)
    {
        await _unitOfWork.Users.SoftDelete(id);
        await _unitOfWork.CompleteAsync();
        return true;
    }
}