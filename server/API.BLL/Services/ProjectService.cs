﻿using API.BLL.DTOs;
using API.BLL.Services.Abstract;
using API.DAL.Context;
using API.DAL.Entities;
using API.DAL.Repository;
using API.DAL.Repository.Project;
using API.DAL.UnitOfWork;
using AutoMapper;




namespace API.BLL.Services;
 
public sealed class ProjectService : BaseService
{
    

    public ProjectService(IMapper mapper, IUnitOfWork unitOfWork ) : base(mapper, unitOfWork)
    {
        
    }

    public async Task<IEnumerable<ProjectDTO>> GetAll()
    {
        var projects = await _unitOfWork.Projects.GetAll();
        return  _mapper.Map<IEnumerable<ProjectDTO>>(projects);
    }

    public async Task<ProjectDTO> GetById(int id)
    {
        var proj = await _unitOfWork.Projects.GetById(id);
        return  _mapper.Map<ProjectDTO>(proj);
    }

    public async Task<bool> Create(ProjectDTO entity)
    {
        var newproj = _mapper.Map<Project>(entity);

        await _unitOfWork.Projects.Create(newproj);
        await _unitOfWork.CompleteAsync();
        return true;

    }

    public async Task<bool> Update(ProjectDTO updProjectDto)
    {
        var updproj = _mapper.Map<Project>(updProjectDto);
        
        await _unitOfWork.Projects.Update(updproj);
        await _unitOfWork.CompleteAsync();
        return true;
    }

    public async Task<bool> Delete(int id)
    {
       await _unitOfWork.Projects.SoftDelete(id);
       await _unitOfWork.CompleteAsync();
       return true;
    }

   
    
    

  
    
}