﻿using API.BLL.DTOs;
using API.BLL.Services.Abstract;
using API.DAL.Context;
using API.DAL.Repository;
using API.DAL.Repository.Task;
using API.DAL.UnitOfWork;
using AutoMapper;
using Task = API.DAL.Entities.Task;

namespace API.BLL.Services;

public class TaskService : BaseService
{
    
   
    public TaskService(IMapper mapper, IUnitOfWork unitOfWork) : base(mapper, unitOfWork)
    {
        
    }

    public async Task<TaskDTO> GetById(int id)
    {
        var task = await _unitOfWork.Tasks.GetById(id);
        return _mapper.Map<TaskDTO>(task);
    }

    public async Task<List<TaskDTO>> GetAll()
    {
        var tasks = await _unitOfWork.Tasks.GetAll();
        return _mapper.Map<List<TaskDTO>>(tasks);
    }

    public async Task<bool> Create(TaskDTO task)
    {
        var newTask = _mapper.Map<Task>(task);
        await _unitOfWork.Tasks.Create(newTask);
        await _unitOfWork.CompleteAsync();
        return true;
    }
    
    public async Task<bool> Update(TaskDTO updTaskDto)
    {
        var updTask = _mapper.Map<Task>(updTaskDto);
        
        await _unitOfWork.Tasks.Update(updTask);
        await _unitOfWork.CompleteAsync();
        return true;
    }

    public async Task<bool> Delete(int id)
    {
        await _unitOfWork.Tasks.SoftDelete(id);
        await _unitOfWork.CompleteAsync();
        return true;
    }
}