﻿using API.BLL.DTOs;
using API.BLL.Services.Abstract;
using API.DAL.Context;
using API.DAL.Entities;
using API.DAL.Repository;
using API.DAL.Repository.Team;
using API.DAL.UnitOfWork;
using AutoMapper;

namespace API.BLL.Services;

public class TeamService : BaseService
{
    
    public TeamService(IMapper mapper, IUnitOfWork unitOfWork) : base(mapper, unitOfWork)
    {
        
    }

    public async Task<List<TeamDTO>> GetAll()
    {
        var teams = await _unitOfWork.Teams.GetAll();
        return _mapper.Map<List<TeamDTO>>(teams);
    }

    public async Task<TeamDTO> GetById(int id)
    {
        var team = await _unitOfWork.Teams.GetById(id);
        return _mapper.Map<TeamDTO>(team);
    }

    public async Task<bool> Create(TeamDTO newTeamDto)
    {
        var newTeam = _mapper.Map<Team>(newTeamDto);
       await _unitOfWork.Teams.Create(newTeam);
       await _unitOfWork.CompleteAsync();
       return true;
    }
    
    public async Task<bool> Update(TeamDTO updTeamDto)
    {
        var updTeam = _mapper.Map<Team>(updTeamDto);
        
        await _unitOfWork.Teams.Update(updTeam);
        await _unitOfWork.CompleteAsync();
        return true;
    }

    public async Task<bool> Delete(int id)
    {
        await _unitOfWork.Teams.SoftDelete(id);
        await _unitOfWork.CompleteAsync();
        return true;
    }
}