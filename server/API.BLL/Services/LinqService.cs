﻿using System.Text.Json;
using API.BLL.DTOs;
using API.BLL.Services.Abstract;
using API.DAL.Entities;
using API.DAL.UnitOfWork;
using AutoMapper;
using Bogus.DataSets;


namespace API.BLL.Services;

public class LinqService: BaseService
{
    public LinqService(IMapper mapper, IUnitOfWork unitOfWork) : base(mapper, unitOfWork)
    {
    }

    public async Task<Dictionary<string,int>> ProjectTasks(int id)
    {
        var projects = await _unitOfWork.Projects.GetProjectWithTask(id);
        var projectDto = _mapper.Map<List<ProjectDTO>>(projects);
        var projectTask = projectDto
            .ToDictionary(data => data.Name, data => data.Tasks.Count);
            
        return projectTask ;
    }

    public async Task<List<TaskDTO>> GetAllTasksUser(int id)
    {
        var tasks = await _unitOfWork.Tasks.GetAll();
        var tasksDto = _mapper.Map<List<TaskDTO>>(tasks);
        var userTasksDto = tasksDto
            .Where(p => p.PerformerId == id)
            .Where(p => p.Name.Length < 45).ToList();

        return userTasksDto;

    }

    public async Task<Dictionary<int, string>> GetFinishedTasksUser(int id)
    {
        var tasks = await _unitOfWork.Tasks.GetAll();
        var tasksDto = _mapper.Map<List<TaskDTO>>(tasks);
        var userFinishedTasks = tasksDto
            .Where(t => t.PerformerId == id)
            .Where(p => p.FinishedAt >= new DateTime(2021, 01, 1) &&
                        p.FinishedAt <= new DateTime(2021, 12, 31))
            .ToDictionary(p => p.Id, p => p.Name);
            
        return userFinishedTasks;

    }

    public async Task<List<IGrouping<TeamDTO?, TeamDTO?>>> GetTeamsWithUsers()
    {
        var teamsUser = await _unitOfWork.Teams.GetAllTeamWithUsers();
        var teamUserDto = _mapper.Map<List<TeamDTO>>(teamsUser);

        var sortedTeamUser = teamUserDto
            .SelectMany(p => p.Users)
            .Where(p => p.BirthDay.Year <= DateTime.UtcNow.AddYears(10).Year)
            .OrderByDescending(p => p.CreatedAt)
            .Select(p => p.Team)
            .GroupBy(p => p ).ToList();
        
        return sortedTeamUser;





    }
    
    
}