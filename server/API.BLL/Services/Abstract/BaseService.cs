﻿using API.DAL.Context;
using API.DAL.UnitOfWork;
using AutoMapper;

namespace API.BLL.Services.Abstract;

public abstract class BaseService
{
    private  protected readonly IMapper _mapper;
    
    private protected readonly IUnitOfWork _unitOfWork;
    
    public  BaseService(IMapper mapper, IUnitOfWork unitOfWork)
    {
        _mapper = mapper;
        _unitOfWork = unitOfWork;
    }
}