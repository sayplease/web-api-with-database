﻿


using API.DAL.Context;
using API.DAL.Entities;
using API.DAL.Repository;
using API.DAL.Repository.Project;
using API.DAL.Repository.Task;
using API.DAL.Repository.Team;
using API.DAL.Repository.User;
using API.DAL.UnitOfWork;
using BSA_WEB_API.Extensions;
using Microsoft.EntityFrameworkCore;

namespace BSA_WEB_API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
    
        public void ConfigureServices(IServiceCollection services)
        {
            var migrationAssembly = typeof(ApiContext).Assembly.GetName().Name;
            services.AddDbContext<ApiContext>(options =>
                options.UseSqlServer(Configuration["ConnectionStrings:ApiDBConnection"], opt => opt.MigrationsAssembly(migrationAssembly)));


            services.AddCors();
            services.AddControllers();
                   
                

            services.RegisterCustomServices();
            services.RegisterAutoMapper();
            services.RegisterRepository();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            app.UseHttpsRedirection();
            app.UseEndpoints(cfg =>
            {
                cfg.MapControllers();
            });
        }
    }
}
