﻿using System.Reflection;
using API.BLL.MapingProfile;
using API.BLL.Services;
using API.DAL.Repository;
using API.DAL.Repository.Project;
using API.DAL.Repository.Task;
using API.DAL.Repository.Team;
using API.DAL.Repository.User;


namespace BSA_WEB_API.Extensions;

public static class ServiceExtensions
{
    public static void RegisterAutoMapper(this IServiceCollection services)
    {
        services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<UserProfile>();
            },
            Assembly.GetExecutingAssembly());
    }

    public static void RegisterCustomServices(this IServiceCollection services)
    {
        services.AddScoped<ProjectService>();
        services.AddScoped<TaskService>();
        services.AddScoped<TeamService>();
        services.AddScoped<UserService>();
        services.AddScoped<LinqService>();

    }

    public static void RegisterRepository(this IServiceCollection services)
    {
        services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
        services.AddScoped<IProjectRepository, ProjectRepository>();
        services.AddScoped<ITaskRepository, TaskRepository>();
        services.AddScoped<ITeamRepository, TeamRepository>();
        services.AddScoped<IUserRepository, UserRepository>();
    }
}