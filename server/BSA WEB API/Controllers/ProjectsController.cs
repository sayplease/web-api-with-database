﻿



using API.BLL.DTOs;
using API.BLL.Services;

using Microsoft.AspNetCore.Mvc;




namespace BSA_WEB_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        private readonly ILogger<ProjectsController> _logger;

        public ProjectsController(ProjectService projectService, ILogger<ProjectsController> logger)
        {
            _projectService = projectService;
            _logger = logger;
        }
        
        [HttpGet]  
        public async Task<IActionResult> GetAllProjects()
        {
            
            return Ok(await _projectService.GetAll());
            
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetProjById(int id)
        {
            var item = await _projectService.GetById(id);

            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> CreateProject(ProjectDTO newProjectDto)
        {
            if (!ModelState.IsValid) return new JsonResult("Somethign Went wrong") {StatusCode = 500};
            await _projectService.Create(newProjectDto);
            return CreatedAtAction("GetProjById", new {newProjectDto.Id}, newProjectDto);

        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> UpdateProject(int id, ProjectDTO updProjectDto)
        {
            if (id != updProjectDto.Id)
                return BadRequest();
            
            await _projectService.Update(updProjectDto);
            return NoContent();
        }
        
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> SoftDeleteProject(int id)
        {
            var item = await _projectService.Delete(id);
            if(item == false)
                return BadRequest();
            return Ok(item);
        }
    }
}
