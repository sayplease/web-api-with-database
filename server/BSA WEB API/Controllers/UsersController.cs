﻿using API.BLL.DTOs;
using API.BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace BSA_WEB_API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UsersController : ControllerBase
{
    private readonly UserService _userService;
    private readonly ILogger<UsersController> _logger;

    public UsersController(UserService userService, ILogger<UsersController> logger)
    {
        _userService = userService;
        _logger = logger;
    }
    
    [HttpGet]  
    public async Task<IActionResult> GetAllUsers()
    {
            
        return Ok(await _userService.GetAll());
            
    }

    [HttpGet("{id:int}")]
    public async Task<IActionResult> GetUserById(int id)
    {
        var item = await _userService.GetById(id);

        return Ok(item);
    }

    [HttpPost]
    public async Task<IActionResult> CreateUser(UserDTO newUserDto)
    {
        if (!ModelState.IsValid) return new JsonResult("Somethign Went wrong") {StatusCode = 500};
        await _userService.Create(newUserDto);
        return CreatedAtAction("GetUserById", new {newUserDto.Id}, newUserDto);

    }

    [HttpPut("{id:int}")]
    public async Task<IActionResult> UpdateUser(int id, UserDTO updUserDto)
    {
        if (id != updUserDto.Id)
            return BadRequest();
            
        await _userService.Update(updUserDto);
        return NoContent();
    }
        
    [HttpDelete("{id:int}")]
    public async Task<IActionResult> SoftDeleteUser(int id)
    {
        var item = await _userService.Delete(id);
        if(item == false)
            return BadRequest();
        return Ok(item);
    }

}