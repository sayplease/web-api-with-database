﻿using API.BLL.DTOs;
using API.BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace BSA_WEB_API.Controllers;
[Route("api/[controller]")]
[ApiController]
public class TasksController : ControllerBase
{
   private readonly TaskService _taskService;
   private readonly ILogger<TasksController> _logger;

   public TasksController(TaskService taskService, ILogger<TasksController> logger)
   {
       _taskService = taskService;
       _logger = logger;
   }
   
   [HttpGet]  
   public async Task<IActionResult> GetAllTasks()
   {
            
       return Ok(await _taskService.GetAll());
            
   }

   [HttpGet("{id:int}")]
   public async Task<IActionResult> GetTaskById(int id)
   {
       var item = await _taskService.GetById(id);

       return Ok(item);
   }

   [HttpPost]
   public async Task<IActionResult> CreateTask(TaskDTO newTaskDto)
   {
       if (!ModelState.IsValid) return new JsonResult("Somethign Went wrong") {StatusCode = 500};
       await _taskService.Create(newTaskDto);
       return CreatedAtAction("GetTaskById", new {newTaskDto.Id}, newTaskDto);

   }

   [HttpPut("{id:int}")]
   public async Task<IActionResult> UpdateTask(int id, TaskDTO updTaskDto)
   {
       if (id != updTaskDto.Id)
           return BadRequest();
            
       await _taskService.Update(updTaskDto);
       return NoContent();
   }
        
   [HttpDelete("{id:int}")]
   public async Task<IActionResult> SoftDeleteTask(int id)
   {
       var item = await _taskService.Delete(id);
       if(item == false)
           return BadRequest();
       return Ok(item);
   }
}