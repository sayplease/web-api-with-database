﻿using API.BLL.DTOs;
using API.BLL.Services;
using API.DAL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace BSA_WEB_API.Controllers;
[Route("api/[controller]")]
[ApiController]
public class LinqsController : ControllerBase
{
    private readonly LinqService _linqService;
    private readonly ILogger<LinqsController> _logger;

    public LinqsController(LinqService linqService, ILogger<LinqsController> logger)
    {
        _linqService = linqService;
        _logger = logger;
    }
    
    [HttpGet("1/{id:int}")]
    public async Task<IActionResult> GetProjTask(int id)
    {
        return Ok (await _linqService.ProjectTasks(id));

    }

    [HttpGet("2/{id:int}")]
    public async Task<IActionResult> GetUserTasks(int id)
    {
        return Ok(await _linqService.GetAllTasksUser(id));
    }

    [HttpGet("3/{id:int}")]
    public async Task<IActionResult> GetFinishedTasks(int id)
    {
        return Ok(await _linqService.GetFinishedTasksUser(id));
    }

    [HttpGet("4")]
    public async Task<IActionResult> GetTeamWithUsers()
    {
        return Ok(await _linqService.GetTeamsWithUsers());
    }
}