﻿using API.BLL.DTOs;
using API.BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace BSA_WEB_API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class TeamsController : ControllerBase
{
    private readonly TeamService _teamService;
    private readonly ILogger<TeamsController> _logger;

    public TeamsController(TeamService teamService, ILogger<TeamsController> logger)
    {
        _teamService = teamService;
        _logger = logger;
    }

    [HttpGet]  
    public async Task<IActionResult> GetAllTeams()
    {
            
        return Ok(await _teamService.GetAll());
            
    }

    [HttpGet("{id:int}")]
    public async Task<IActionResult> GetTeamById(int id)
    {
        var item = await _teamService.GetById(id);

        return Ok(item);
    }

    [HttpPost]
    public async Task<IActionResult> CreateTeam(TeamDTO newTeamDto)
    {
        if (!ModelState.IsValid) return new JsonResult("Somethign Went wrong") {StatusCode = 500};
        await _teamService.Create(newTeamDto);
        return CreatedAtAction("GetTeamById", new {newTeamDto.Id}, newTeamDto);

    }

    [HttpPut("{id:int}")]
    public async Task<IActionResult> UpdateTeam(int id, TeamDTO updTeamDto)
    {
        if (id != updTeamDto.Id)
            return BadRequest();
            
        await _teamService.Update(updTeamDto);
        return NoContent();
    }
        
    [HttpDelete("{id:int}")]
    public async Task<IActionResult> SoftDeleteTeam(int id)
    {
        var item = await _teamService.Delete(id);
        if(item == false)
            return BadRequest();
        return Ok(item);
    }
}