﻿using API.DAL.Context;
using API.DAL.Repository.Task;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace API.DAL.Repository.Team;

public class TeamRepository : GenericRepository<Entities.Team>, ITeamRepository
{
    public TeamRepository(ApiContext context, ILogger logger) : base(context, logger) {}

    public async Task<bool> SoftDelete(int id)
    {
        try
        {
            var teamDelete = await _context.Teams.Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            if (teamDelete == null) return false;

            teamDelete.IsDeleted = true;
            return true;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "{Repo} Delete Funciton error", typeof(TaskRepository));
            return false;
        }
    }

    public async Task<List<Entities.Team>> GetAllTeamWithUsers()
    {
        return await _context.Teams.Include(p => p.Users).ToListAsync();

    }

    public override async Task<bool> Update(Entities.Team entity)
    {
        try
        {
            var updTeam = await _context.Teams.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();
            if (updTeam == null) return await Create(entity);

            
            updTeam.Name = entity.Name;
            updTeam.UpdatedAt = DateTime.Now;
                
            return true;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "{Repo} Update funciton error", typeof(TeamRepository) );
            return false;
        }
    }
}

