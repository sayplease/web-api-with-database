﻿

namespace API.DAL.Repository.Team;

public interface ITeamRepository : IGenericRepository<Entities.Team>
{
    Task<bool> SoftDelete(int id);

    Task<List<Entities.Team>> GetAllTeamWithUsers();

}