﻿using System.Linq.Expressions;
using API.DAL.Context;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace API.DAL.Repository;

public class GenericRepository<T> : IGenericRepository<T> where T : class
{
    protected readonly ApiContext _context;
    public readonly ILogger _logger;

    public GenericRepository(ApiContext context, ILogger logger)
    {
        _context = context;
        _logger = logger;
    }

    public virtual  async Task<IEnumerable<T>> GetAll()
    {
        return await _context.Set<T>().ToListAsync();
    }

    public virtual  async Task<T> GetById(int id)
    {

        return await _context.Set<T>().FindAsync(id);
    }

    public virtual async Task<bool> Create(T entity)
    {
        await _context.Set<T>().AddAsync(entity);
        return true;
    }

    public virtual Task<bool> Update(T entity)
    {
        throw new NotImplementedException();
    }

    public async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate)
    {
        return await _context.Set<T>().Where(predicate).ToListAsync();
    }
}