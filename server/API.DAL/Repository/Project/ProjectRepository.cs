﻿using System.Linq;
using API.DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace API.DAL.Repository.Project;
    public class ProjectRepository : GenericRepository<Entities.Project>, IProjectRepository
    {
        public ProjectRepository(ApiContext context, ILogger logger) : base(context, logger) {}

        public async Task<bool> SoftDelete(int id)
        {
            try
            {
                var projDelete = await _context.Projects.Where(x => x.Id == id)
                    .FirstOrDefaultAsync();
                if (projDelete == null) return false;

                projDelete.IsDeleted = true;
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{Repo} Delete Funciton error", typeof(ProjectRepository));
                return false;
            }
        }

        public async Task<List<Entities.Project>> GetProjectWithTask(int id)
        {
            return await _context.Projects.Where(p => p.AuthorId == id)
                .Include(p => p.Tasks).ToListAsync();
        }

        public override async Task<bool> Update(Entities.Project entity)
        {
            try
            {
                var updProject = await _context.Projects.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();
                if (updProject == null) return await Create(entity);

                updProject.Deadline = entity.Deadline;
                updProject.Description = entity.Description;
                updProject.Name = entity.Name;
                updProject.TeamId = entity.TeamId;
                updProject.UpdatedAt = DateTime.Now;

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{Repo} Update funciton error", typeof(ProjectRepository) );
                return false;
            }
        }

    }

