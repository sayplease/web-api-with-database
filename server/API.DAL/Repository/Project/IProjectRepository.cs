﻿

namespace API.DAL.Repository.Project;

public interface IProjectRepository : IGenericRepository<Entities.Project>
{
    Task<bool> SoftDelete(int id);
    Task<List<Entities.Project>> GetProjectWithTask(int id);
}