﻿

namespace API.DAL.Repository.User;

    public interface IUserRepository : IGenericRepository<Entities.User>
    {
        Task<bool> SoftDelete(int id);
        
    }
