﻿

using API.DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace API.DAL.Repository.User
{
    public class UserRepository : GenericRepository<Entities.User>, IUserRepository
    {
        public UserRepository(ApiContext context, ILogger logger) : base(context, logger)
        {
        }


        public async Task<bool> SoftDelete(int id)
        {
            try
            {
                var userDelete = await _context.Users.Where(x => x.Id == id)
                    .FirstOrDefaultAsync();
                if (userDelete == null) return false;

                userDelete.IsDeleted = true;
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{Repo} Delete Funciton error", typeof(UserRepository));
                return false;
            }
        }
        public override async Task<bool> Update(Entities.User entity)
        {
            try
            {
                var updUser = await _context.Users.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();
                if (updUser == null) return await Create(entity);

                updUser.FirstName = entity.FirstName;
                updUser.LastName = entity.LastName;
                updUser.BirthDay = entity.BirthDay;
                updUser.UpdatedAt = DateTime.Now;
                
                
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{Repo} Update funciton error", typeof(UserRepository) );
                return false;
            }
        }
    }
}
