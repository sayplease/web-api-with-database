﻿

using API.DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace API.DAL.Repository.Task
{
    public class TaskRepository : GenericRepository<Entities.Task>, ITaskRepository
    {
        public TaskRepository(ApiContext context, ILogger logger) : base(context, logger) {}

        public async Task<bool> SoftDelete(int id)
        {
            try
            {
                var taskDelete = await _context.Tasks.Where(x => x.Id == id)
                    .FirstOrDefaultAsync();
                if (taskDelete == null) return false;


                taskDelete.IsDeleted = true;
                
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{Repo} Delete Funciton error", typeof(TaskRepository));
                return false;
            }
        }
        public override async Task<bool> Update(Entities.Task entity)
        {
            try
            {
                var updTask = await _context.Tasks.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();
                if (updTask == null) return await Create(entity);

                updTask.Description = entity.Description;
                updTask.Name = entity.Name;
                updTask.State = entity.State;
                updTask.UpdatedAt = DateTime.Now;
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{Repo} Update funciton error", typeof(TaskRepository) );
                return false;
            }
        }
    }
}
