﻿namespace API.DAL.Repository.Task;

public interface ITaskRepository : IGenericRepository<Entities.Task>
{
    Task<bool> SoftDelete(int id);
}