﻿using API.DAL.Repository.Project;
using API.DAL.Repository.Task;
using API.DAL.Repository.Team;
using API.DAL.Repository.User;

namespace API.DAL.UnitOfWork;

public interface IUnitOfWork : IDisposable
{
    IProjectRepository Projects { get; }
    ITaskRepository Tasks { get; }
    ITeamRepository Teams { get; }
    IUserRepository Users { get; }
    Task CompleteAsync();
}