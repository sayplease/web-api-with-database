﻿using API.DAL.Context;
using API.DAL.Repository.Project;
using API.DAL.Repository.Task;
using API.DAL.Repository.Team;
using API.DAL.Repository.User;
using Microsoft.Extensions.Logging;

namespace API.DAL.UnitOfWork;

public class UnitOfWork :IUnitOfWork
{
    private readonly ApiContext _context;
    private readonly ILogger _logger;

    public UnitOfWork(ApiContext context, ILoggerFactory loggerFactory)
    {
        _context = context;
        _logger = loggerFactory.CreateLogger("logs");
        
        Projects = new ProjectRepository(_context, _logger);
        Tasks = new TaskRepository(_context, _logger);
        Teams = new TeamRepository(_context, _logger);
        Users = new UserRepository(_context, _logger);
    }

    public IProjectRepository Projects { get; }
    public ITaskRepository Tasks { get; }
    public ITeamRepository Teams { get; }
    public IUserRepository Users { get; }
    public async Task CompleteAsync()
    {
        await _context.SaveChangesAsync();
    }
    public void Dispose()
    {
        _context.DisposeAsync();
    }
}