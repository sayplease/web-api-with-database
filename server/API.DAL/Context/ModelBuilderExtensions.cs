﻿using API.DAL.Entities;
using API.DAL.EnumState;
using Bogus;
using Microsoft.EntityFrameworkCore;
using Task = API.DAL.Entities.Task;

namespace API.DAL.Context;

public static class ModelBuilderExtensions
{
    private const int ENTITY_COUNT = 100;
    public static void Configure(this ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Task>()
            .HasQueryFilter(s => !s.IsDeleted)
            .HasOne(p => p.Performer)
            .WithMany(p => p.Tasks)
            .HasForeignKey(p => p.PerformerId)
            .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<Team>()
            .HasQueryFilter(s => !s.IsDeleted)
            .HasMany(p => p.Users)
            .WithOne(p => p.Team)
            .HasForeignKey(p => p.TeamId)
            .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<Task>()
            .HasQueryFilter(s => !s.IsDeleted)
            .HasOne(p => p.Project)
            .WithMany(p => p.Tasks)
            .HasForeignKey(p => p.ProjectId)
            .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<User>()
            .HasQueryFilter(s => !s.IsDeleted);


        modelBuilder.Entity<Project>()
            .HasQueryFilter(s => !s.IsDeleted)
            .HasOne(p => p.Author)
            .WithMany(p => p.Projects)
            .HasForeignKey(p => p.AuthorId)
            .OnDelete(DeleteBehavior.Restrict);





    }

    public static void Seed(this ModelBuilder modelBuilder)
    {
        var teams = GenerateRandomTeams();
        var users = GenerateRandomUsers(teams);
        var projects = GenerateRnadomProjects(users, teams);
        var tasks = GenerateRandomTasks(users, projects);

        modelBuilder.Entity<Team>().HasData(teams);
        modelBuilder.Entity<User>().HasData(users);
        modelBuilder.Entity<Project>().HasData(projects);
        modelBuilder.Entity<Task>().HasData(tasks);
    }
    public static List<Team> GenerateRandomTeams()
    {
        int teamId = 1;

        var testTeamsFake = new Faker<Team>()
            .RuleFor(t => t.Id, f => teamId++)
            .RuleFor(t => t.Name, f => f.Internet.DomainName())
            .RuleFor(t => t.CreatedAt, f => DateTime.Now)
            .RuleFor(t => t.UpdatedAt, f => DateTime.Now);

        var generateTeams = testTeamsFake.Generate(ENTITY_COUNT);
        return generateTeams;

    }

    public static List<User> GenerateRandomUsers(List<Team> teams)
    {
        int userId = 1;

        var testUsersFake = new Faker<User>()
            .RuleFor(u => u.Id, f => userId++)
            .RuleFor(u => u.Email, f => f.Internet.Email())
            .RuleFor(u => u.FirstName, f => f.Name.FirstName())
            .RuleFor(u => u.LastName, f => f.Name.LastName())
            .RuleFor(u => u.BirthDay, f => f.Date.Past(yearsToGoBack: 18))
            .RuleFor(u => u.CreatedAt, f => DateTime.Now)
            .RuleFor(u => u.UpdatedAt, f => DateTime.Now)
            .RuleFor(u => u.TeamId, f => f.PickRandom(teams).Id);


        return testUsersFake.Generate(ENTITY_COUNT);
    }

    public static List<Project> GenerateRnadomProjects(List<User> users, List<Team> teams)
    {
        int projectId = 1;

        var testProjectFake = new Faker<Project>()
            .RuleFor(p => p.Id, f => projectId++)
            .RuleFor(p => p.Name, f => f.Name.JobArea())
            .RuleFor(p => p.Description, f => f.Lorem.Text())
            .RuleFor(p => p.Deadline, f => f.Date.Future())
            .RuleFor(p => p.CreatedAt, f => DateTime.Now)
            .RuleFor(p => p.UpdatedAt, f => DateTime.Now)
            .RuleFor(p => p.AuthorId, f => f.PickRandom(users).Id)
            .RuleFor(p => p.TeamId, f => f.PickRandom(teams).Id);

        return testProjectFake.Generate(ENTITY_COUNT);
    }

    public static List<Task> GenerateRandomTasks(List<User> users, List<Project> projects)
    {
        int taskId = 1;

        var testTasksFake = new Faker<Task>()
            .RuleFor(t => t.Id, f => taskId++)
            .RuleFor(t => t.Name, f => f.Name.JobTitle())
            .RuleFor(t => t.Description, f => f.Lorem.Text())
            .RuleFor(t => t.State, TaskState.Done)
            .RuleFor(t => t.CreatedAt, f => DateTime.Now)
            .RuleFor(t => t.UpdatedAt, f => DateTime.Now)
            .RuleFor(t => t.ProjectId, f => f.PickRandom(projects).Id)
            .RuleFor(t => t.PerformerId, f => f.PickRandom(users).Id)
            .RuleFor(t => t.FinishedAt, f => f.Date.Past());

        return testTasksFake.Generate(ENTITY_COUNT);
    }
}