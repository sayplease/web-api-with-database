﻿using API.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Task = API.DAL.Entities.Task;

namespace API.DAL.Context;

public class ApiContext : DbContext
{
    public DbSet<User> Users { get; private set; }
    public DbSet<Project> Projects { get; private set; }
    public DbSet<Task> Tasks { get; private set; }
    public DbSet<Team> Teams { get; private set; }

    public ApiContext(DbContextOptions<ApiContext> options) : base(options) {}
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Setting up entities using extension method
        modelBuilder.Configure();

        // Seeding data using extension method
        // NOTE: this method will be called every time after adding a new migration, cuz we use Bogus for seed data
        modelBuilder.Seed();
    }
}