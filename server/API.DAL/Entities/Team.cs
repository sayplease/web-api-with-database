﻿namespace API.DAL.Entities
{
    public class Team : BaseEntity.BaseEntity
    {
        public string? Name { get; set; }

        public List<User> Users { get; set; } 

        public List<Project> Projects { get; set; }
        
    }
}
