﻿namespace API.DAL.Entities
{
    public class User : BaseEntity.BaseEntity
    {

        public int? TeamId { get; set; }
        
        public Team? Team { get; set;}
        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public string? Email { get; set; }
        
        public DateTime BirthDay { get; set; }

        public List<Task> Tasks { get; set; } 

        public List<Project> Projects { get; set; }
 
    }
}
