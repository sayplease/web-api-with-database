﻿namespace API.DAL.Entities
{
    public sealed class Project : BaseEntity.BaseEntity
    {
        public int AuthorId { get; set; }
        
        public User? Author { get; set; }

        public int TeamId { get; set; }

        public string? Name { get; set; }
        
        public Team? Team { get; set; }

        public string? Description { get; set; }

        public DateTime? Deadline { get; set; }

        public List<Task> Tasks { get; private set; } 

        public List<User> Users { get; private set;} 


    }
}
