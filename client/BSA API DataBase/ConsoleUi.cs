﻿using BSA_API_DataBase.Base;
using BSA_API_DataBase.Services;
using BSA_API_DataBase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA_API_DataBase
{
    public class ConsoleUi
    {
        private static bool _isExit = false;

        public static async System.Threading.Tasks.Task Run()
        {
            var data = new BaseService(new HttpClient());
            var queries = new BaseQueries(await data.GetDataHierarchyAsync());

            while (!_isExit)
            {
                Console.WriteLine("Запросы: ");
                Console.WriteLine(" 1 -Получить кол-во тасков у проекта конкретного пользователя (по id) (список из пост-количество)");
                Console.WriteLine(" 2 - Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов (коллекция из тасков)");
                Console.WriteLine(" 3 - Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2021) году для конкретного пользователя (по id)");
                Console.WriteLine(" 4 - Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет," +
                " отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.");
                Console.WriteLine(" 5 - Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию)");
                Console.WriteLine(" 6 - Получить структуру User (передать Id пользователя в параметры)");
                Console.WriteLine(" 7 - Выход");
                Console.Write("Выберите действие: ");

                int choice = 0;
                int id;

               choice = int.Parse(Console.ReadLine());
                
                switch (choice)
                {
                    case 1:
                        Console.Write("Введите id пользователя: ");
                        id = int.Parse(Console.ReadLine());
                        var query1Result =  queries.GetTasksFromUser(id);
                        Console.WriteLine("=========================================");
                        if (query1Result.Count == 0)
                        {
                            Console.WriteLine("Данных нет :(");
                        }
                        else
                        {
                            foreach (var record in query1Result)
                            {
                                Console.WriteLine($"Проект \"{record.Project.Name}\" имеет {record.Tasks} Task");
                            }
                        }
                        Console.WriteLine("=========================================");
                        Console.WriteLine("Нажмите любую клавишу для продолжения...");
                        Console.ReadKey();
                        Console.Clear();
                        break;

                    case 2:
                        Console.Write("Введите id пользователя: ");
                        id = int.Parse(Console.ReadLine());
                        var query2Result = queries.GetTaskWithName(id);
                        Console.WriteLine("=========================================");
                        if (query2Result.Count == 0)
                        {
                            Console.WriteLine("Данных нет :(");
                        }
                        else
                        {
                            foreach (var record in query2Result)
                            {
                                Console.WriteLine($"Название такса \"{record.Name}\" имеет {record.Name.Length} символов");
                            }
                        }
                        Console.WriteLine("=========================================");
                        Console.WriteLine("Нажмите любую клавишу для продолжения...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Console.Write("Введите id пользователя: ");
                        id = int.Parse(Console.ReadLine());
                        var query3Result = queries.GetTaskWithIdAndName(id);
                        Console.WriteLine("=========================================");
                        if (query3Result.Count == 0)
                        {
                            Console.WriteLine("Данных нет :(");
                        }
                        else
                        {
                            foreach (var record in query3Result)
                            {
                                Console.WriteLine($"Id: {record.Id} | Task \"{record.Name}\" Finished");
                            }
                        }
                        Console.WriteLine("=========================================");
                        Console.WriteLine("Нажмите любую клавишу для продолжения...");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                }
            }
        }
    }

}
