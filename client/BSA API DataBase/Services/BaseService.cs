﻿using BSA_API_DataBase.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA_API_DataBase.Services
{
    public class BaseService
    {
        private readonly HttpClient _client;
        private string baseUrl = "https://localhost:7219/api/";

        public BaseService(HttpClient client)
        {
            _client = client;
        }

        private async Task<List<T>> GetDataCollectionAsync<T>(string endpoint) where T : class
        {
            return JsonConvert.DeserializeObject<List<T>>(
                await _client.GetStringAsync(baseUrl + endpoint));
        }
        public async Task<List<Project>> GetDataHierarchyAsync()
        {
            var users = await GetDataCollectionAsync<User>("Users");
            var projects = await GetDataCollectionAsync<Project>("Projects");
            var tasks = await GetDataCollectionAsync<Models.Task>("Tasks");
            var teams = await GetDataCollectionAsync<Team>("Teams");

             #region Binding collections
             var dataHierarchy = (from project in projects
                                  join task in tasks on project.Id equals task.ProjectId into taskGroup
                                  join team in teams on project.TeamId equals team.Id into teamGroup
                                  join user in users on project.AuthorId equals user.Id into authorGroup
                                  select new Project
                                  {
                                      Id = project.Id,
                                      AuthorId = project.AuthorId,
                                      TeamId = project.TeamId,
                                      Name = project.Name,
                                      Deadline = project.Deadline,
                                      Description = project.Description,
                                      CreatedAt = project.CreatedAt,

                                      Tasks = (from innerProjectTask in taskGroup
                                               join user in users on innerProjectTask.PerformerId equals user.Id into performerTaskGroup
                                               select new Models.Task
                                               {
                                                   Id = innerProjectTask.Id,
                                                   Name = innerProjectTask.Name,
                                                   Description = innerProjectTask.Description,
                                                   CreatedAt = innerProjectTask.CreatedAt,
                                                   FinishedAt = innerProjectTask.FinishedAt,
                                                   PerformerId = innerProjectTask.PerformerId,
                                                   ProjectId = innerProjectTask.ProjectId,
                                                   State = innerProjectTask.State,

                                                   Performers = (from innerPerformer in performerTaskGroup
                                                                 select new User
                                                                 {
                                                                     Id = innerPerformer.Id,
                                                                     FirstName = innerPerformer.FirstName,
                                                                     LastName = innerPerformer.LastName,
                                                                     Email = innerPerformer.Email,
                                                                     TeamId = innerPerformer.TeamId,
                                                                     RegisteredAt = innerPerformer.RegisteredAt,
                                                                     BirthDay = innerPerformer.BirthDay,

                                                                 }).ToList(),

                                               }).ToList(),


                                      Author = (from innerAuthor in authorGroup
                                                select new User
                                                {
                                                    Id = innerAuthor.Id,
                                                    FirstName = innerAuthor.FirstName,
                                                    LastName = innerAuthor.LastName,
                                                    Email = innerAuthor.Email,
                                                    TeamId = innerAuthor.TeamId,
                                                    RegisteredAt = innerAuthor.RegisteredAt,
                                                    BirthDay = innerAuthor.BirthDay,

                                                }).ToList(),

                                      Teams = (from innerProjectTeam in teamGroup
                                          join user in users on innerProjectTeam.Id equals user.TeamId into userTeamGroup
                                               select new Team
                                               {
                                                   Id = innerProjectTeam.Id,
                                                   Name = innerProjectTeam.Name,
                                                   CreatedAt = innerProjectTeam.CreatedAt,
                                                   Users = (from innerUserTeam in userTeamGroup
                                                       select new User
                                                       {
                                                           Id = innerUserTeam.Id,
                                                           Email = innerUserTeam.Email,
                                                           BirthDay = innerUserTeam.BirthDay,
                                                           FirstName = innerUserTeam.FirstName,
                                                           LastName = innerUserTeam.LastName,
                                                           RegisteredAt = innerUserTeam.RegisteredAt,
                                                           TeamId = innerUserTeam.TeamId
                                                               
                                                       }).ToList()
                                                   
                                               }).ToList()

                                  }).ToList();
             
            #endregion


            return dataHierarchy;

        }
    }
}
