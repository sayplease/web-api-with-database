﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BSA_API_DataBase.Models
{
    public class Team
    {
        
        public int Id { get; set; }

        
        public string Name { get; set; }

        
        public DateTime? CreatedAt { get; set; }
        
        public List<User> Users { get; set; }
    }
}
