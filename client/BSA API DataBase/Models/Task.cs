﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BSA_API_DataBase.Models
{
    public class Task
    {
        
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int PerformerId { get; set; }
 
        public string Name { get; set; }
        
        public string? Description { get; set; }

        public TaskState State { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? FinishedAt { get; set; }

        public List<User> Performers { get; set; }

        public List<Project> Projects { get; set; }
    }
    public enum TaskState
    {
        Create = 0,

         ToDo= 1,

        Testing = 2,

        Done = 3,

    }
}
