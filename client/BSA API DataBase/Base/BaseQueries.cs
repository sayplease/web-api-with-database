﻿using BSA_API_DataBase.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Task = BSA_API_DataBase.Models.Task;


namespace BSA_API_DataBase.Base
{
    public class BaseQueries
    {
        private readonly List<Project> _baseHierarchy;
        public BaseQueries(List<Project> baseHierarchy)
        {
            _baseHierarchy = baseHierarchy;
        }

        public  List<(Project Project, int Tasks)> GetTasksFromUser(int userId)
        {
            return _baseHierarchy.Select(x => (x, x.Tasks.Count(y => y.PerformerId == userId))).ToList();
        }


        public List<Models.Task> GetTaskWithName(int userId)
        {
            return _baseHierarchy.SelectMany(p => p.Tasks)
                .Where(p => p.PerformerId == userId)
                .Where(p => p.Name.Length < 45).ToList();
        }

        public List<(int Id, string Name)> GetTaskWithIdAndName(int userId)
        {
            return _baseHierarchy.SelectMany(p => p.Tasks)
                .Where(p => p.PerformerId == userId)
                .Where(s => s.FinishedAt == new DateTime(2021))
                .Select(x => (x.Id, x.Name)).ToList();
        }


    }
    
}
